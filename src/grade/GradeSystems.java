package grade;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * class GradeSystems 儲存 a list of student grades. </br>
 * 
 * GradeSystems () //建構子  </br>
 * 
 * public method:   </br>
 * 		hasID(ID)
 * 		showGrade(ID) </br>
 * 		showRank(ID)  </br>
 * 		updateWeights() </br>
 * 
 */

public class GradeSystems {

	private double[] weights;

	private LinkedList<Grades> aList;
	private final String filePath = System.getProperty("user.dir") +"/src/InputFile3"; //input file path
	private final String[] gradeName = {"lab1","lab2","lab3","mid-term","final exam","total grade"}; //string constant 
	private Scanner scanner = new Scanner(System.in);
	
	
	
	/**
	 * GradeSystems Constructor
	 * @code
	 * 1.initial private variable <br/>
	 * 2.buildGradeList throw IOException
	 */
	public GradeSystems() {
		
		aList = new LinkedList<Grades>();
	
		weights = new double[5];
		//set default weight
		weights[0] = 0.1;
		weights[1] = 0.1;
		weights[2] = 0.1;
		weights[3] = 0.3;
		weights[4] = 0.4;
		
		try {
			buildGradeList();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}// end of constructor
	
	
/**
 * Read input file into aList
 *  
 * @throws IOException
 * 
 * @code
 * 	1.開檔 input file </br>
 * 	2. while not endOfFile </br>
 * 		1. call Grades() 建構aGrade </br>
 * 		2. 用 Java Scanner 來 scan line 把各欄位存入aGrade </br>
 * 		3. aGrade.calculateTotalGrade(weights) 回傳aTotalGrade把它存入aGrade </br>
 * 		4. 把 aGrade 存入 aList </br>
 * 		5. if endOfLine then read line end if </br>
 * 	endWhile
 */
	private void buildGradeList() throws IOException {

		File fr = null;

		fr = new File(filePath);

		Scanner fileScanner = new Scanner(fr);

		
		//start Read file
		while (fileScanner.hasNextLine()) {
			String line = fileScanner.nextLine();
			
			Grades aGrade = new Grades();
			Scanner lineScanner = new Scanner(line); 
			lineScanner.useDelimiter(" ");
			
			aGrade.setId(lineScanner.next());
			aGrade.setName(lineScanner.next());
			
			for  (int i=0;i<5 && lineScanner.hasNext();i++) {
				aGrade.setGradeItem(i, lineScanner.next());
				
			}
			
			
			aGrade.calculateTotalGrade(weights);
			
			aList.add(aGrade);
			lineScanner.close();
		} //end of while
		fileScanner.close();
	}//end of buildGradeList
	

/**
 * print 此id 的所有成績
 *
 * @param id
 * @code 
 * 	1.use hasID to find index of id in aList </br>
 * 	2.print every grade 
 * 
 */
	public void showGrade(String id) {
		
		int index = hasID(id);
		Grades aGrade = aList.get(index);
		
		System.out.printf("%s成績:\n", aGrade.getName());
		for (int i = 0; i < aGrade.gradeItem.length; i++)
			System.out.printf("%s: \t%d\n",gradeName[i], aGrade.gradeItem[i]);

		System.out.print(gradeName[5]+": \t"+aGrade.totalGrade+"\n");

	}//end of showGrade
	
	
/**
 * 確認是否有此ID,若找到回傳其index,沒有回傳-1
 * 
 * @param id
 * @return index of id
 * 
 * @code
 * 	1.scan whole aList
 * 	2.若沒有找到id 則return -1
 * 
 */
	public int hasID(String id) {
		
		
		for (int i = 0; i < aList.size(); i++) {
			
			if (aList.get(i).getId().equals(id)) 
				return i;
			
		}//end of for

		return -1; // not found

	}//end of hasID
	
/**
 * 印出此id的排名
 * @param id
 * 
 * @code
 * 	1.use hasID get index of id in aList </br>
 * 	2.取得這id的totalGrade ,令rank =1 </br>
 *  3. loop aGrade in aList if aTotalGrade > theTotalGrade then rank加1(退1名) end loop </br>
 *  4.印出其rank
 *  
 */
	public void showRank(String id) {
		
		int index = hasID(id);
		int theTotalGrade = aList.get(index).totalGrade;
		int rank = 1;
		
		for (Grades aGrade :aList ) {
			if (aGrade.totalGrade > theTotalGrade)
				rank++;
		}
		
		System.out.printf("%s排名第%d\n", aList.get(index).getName(), rank);
		
	}// end of showRank


	/**
	 * Update Weight
	 * 
	 * @return true if update complete
	 * @code
	 * 1. showOldWeight()
	 * 2. getNewWeights()
	 * 3. setWeights(weights)
	 */
	public boolean updateWeights() {
		
		double[] newWeights;
		
		showOldWeights();
		
		while(true){
			
			newWeights = getNewWeights();
			
			System.out.println();
			//check weight			
			System.out.println("請確認新配分");
			for (int i = 0; i < newWeights.length; i++) {
				System.out.println(gradeName[i]+"\t"+newWeights[i]+"%");
			}
			
			System.out.println("以上正確嗎？Y(Yes) 或 N(No)");
			String in = scanner.nextLine();
			
			if(in.equalsIgnoreCase("Y")||in.equalsIgnoreCase("yes"))
				break;
		}
		
		
		setWeights(newWeights);
				
		
		return true;
	}
	
	
	/**
	 * print Old weights
	 */
	private void showOldWeights(){
			System.out.println("舊配分");
			for (int i = 0; i < weights.length; i++) {
				System.out.println(gradeName[i]+"\t"+weights[i]*100+"%");
			}
	}
	
	/**
	 * Get new Weights from user input, and check
	 * @return newWeight array
	 * @code
	 * 1. print "輸入新配分"
	 * 2. get user input
	 * 3. check sum of newWeights equal 100
	 */
	private double [] getNewWeights(){
		double[] newWeight = new double[5];
		//Scanner scanner = new Scanner(System.in);
		
		boolean weightValid =false; 
		while(!weightValid){
			System.out.println("輸入新配分：   eg.30");

			//get user input
			for (int i = 0; i < newWeight.length; i++) {
				
				System.out.println(gradeName[i]+"\t");
				newWeight[i] = scanner.nextInt();
				scanner.nextLine();

			}
			
			
			//check newWeights
			int sum = 0;
			for (double d : newWeight) {
				sum +=d;
			}
			if(sum != 100)	
				showWeightErrorMsg();
			else 
				weightValid = true;
			
		}//end of while
		
		
		return newWeight;
		
	}// end of getNewWeights
	
	private void showWeightErrorMsg(){
		
		System.out.println("輸入配分格式錯誤!!!");
		System.out.println("請重新輸入.");
	}
	private void setWeights(double newWeights[]){
		for (int i = 0; i < weights.length; i++) {
			weights[i] = newWeights[i]/100;
		}
	}

}
