package grade;

import java.io.IOException;

/**
 * 本Grade system 讓使用者(學生)取得他的總成績total grade 及排名 rank. Total grade 基於配分weights 來算
 * 而 weights可以update. Rank 表示此 total grade 在全班學生的分數排序
 * 
 * Input file: 全班學生的分數 例如 962001044 凌宗廷 87 86 98 88 87 962001051 李威廷 81 98 84 90
 * 93 注意 data field names 如下: ID name lab1 lab2 lab3 midTerm finalExam
 */

public class GradeSystemProject extends Object {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws NoSuchIDExceptions 
	 * @throws NoSuchCommandExceptions 
	 */
	public static void main(String[] args)  {
		// TODO Auto-generated method stub
		
		
	
		try {
			UI aUI = new UI();
		} catch (NoSuchIDExceptions e) {
			// TODO Auto-generated catch block
			System.out.println(e.errorMsg);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchCommandExceptions e) {
			// TODO Auto-generated catch block
			System.out.println(e.errorMsg);
		}
	
		

	}
	
	public static class NoSuchIDExceptions extends Exception{
		String errorMsg;
		public NoSuchIDExceptions(String message) {
			// TODO Auto-generated constructor stub
			
			super(message);
			errorMsg = message;
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
	
	}
	
	
	public static class NoSuchCommandExceptions extends Exception{

		/**
		 * 
		 */
		
		
		private static final long serialVersionUID = 1L;
		String errorMsg;
		public NoSuchCommandExceptions(String message) {
			// TODO Auto-generated constructor stub
			super(message);
			errorMsg = message;
		}
	
	
	}

}
