package grade;

import java.io.IOException;
import java.util.Scanner;


import grade.GradeSystemProject.NoSuchIDExceptions;
import grade.GradeSystemProject.NoSuchCommandExceptions;;

/**
 * 
 *  class UI (user interface) 
 * 	checkID(ID)
 *	promptCommand()
 *	promptID()
 *	showFinishMsg()
 *	showWelcomeMsg()
 *	UI() 建構子 建構 aGradeSystem
 * 
 */
public class UI {
	private static String ID = null;
	private static  String NAME = null;
	private GradeSystems aGradeSystem;


	/**
	 * 
	 * UI() constructor
	 * @throws NoSuchIDExceptions
	 * @throws IOException
	 * @throws NoSuchCommandExceptions
	 * @code
	 * 1.call GradeSystems() to建構 aGradeSystem <br/>
	 * 2.loop1 until Q (Quit)	<br/>
	 * 	2.1promptID() to get user ID  輸入ID或 Q (結束使用)? <br/> 
	 *	2.2.	checkID (ID) 看 ID 是否在 aGradeSystem內 <br/>
	 *	2.3.	showWelcomeMsg (ID)      ex. Welcome李威廷 <br/>
	 *	2.4	loop2 until E (Exit) <br/>
	 *			promptCommand() to prompt for inputCommand <br/> 
	 *   	end loop2 <br/>
	 *	end loop1 <br/>
	 *
	 */
	public UI() throws NoSuchIDExceptions, IOException, NoSuchCommandExceptions{
		
		try{

			
			aGradeSystem = new GradeSystems();

			boolean quitFlag = false;
			while(!quitFlag){
				String id = promptID();
				
				if(id.equalsIgnoreCase("Q")){
					quitFlag = true;
					showFinishMsg();
					break;
				}
					 		
					checkID(id);
					showWelcomeMsg();
					promptCommand();
				

			} //end of while
		}finally{	

		}
	}// end of UI constructor
	
	
	/**
	 * checkID(ID) 
	 * @throws NoSuchIDExceptions
	 * @para: ID a user ID ex: 123456789
	 * @return Boolean true if 有此id ,false if 沒有此id
	 * @time: O(n) n is aGradeSystem 內全班人數
	 *
	 */
	public boolean checkID(String id) throws NoSuchIDExceptions {
		
		if(aGradeSystem.hasID(id)>0)
		{
			this.ID = id;
			return true;
		}else
			throw  new NoSuchIDExceptions("[error]ID"+id+"錯了");
	
	}// end of checkID

	/**
	 * promptCommand() 
	 *  Print Command 選單
	 * @return 0 for normal state
	 * @throws IOException
	 * @throws NoSuchCommandExceptions
	 * @code
	 * 1.prompt user for inputCommand <br/>
	 * 2. if inputCommand is not G (Grade),R (Rank), W (Weights), or E (Exit), <br/>
	 * 			throws an object of NoSuchCommandException <br/>
	 * 3. if inputCommand is E (Exit) then break <br/>
	 *	else: G aGradeSystem.showGrade(ID), R showRank(ID), W updateWeights() end if <br/>
	 *
	 */
	public int promptCommand() throws IOException,NoSuchCommandExceptions {
	
		Scanner scanner = new Scanner(System.in);
		String inputCommand = new String();
		boolean exitFlag = false;
		
		while(!exitFlag)
		{
			System.out.printf("\n*******************************\n");
			System.out.printf(" 輸入指令\n");
			System.out.printf(" \t 1) G 顯示成績(Grade)\n");
			System.out.printf(" \t 2) R 顯示排名(Rank)\n");
			System.out.printf(" \t 3) W 更新配分(Weight)\n");
			System.out.printf(" \t 4) E 離開選單(Exit)\n");
			System.out.printf("*******************************\n");
			System.out.print(">");
			
			
			//if(scanner.hasNextLine())
				inputCommand = scanner.nextLine().toUpperCase();
			//else break;
			
			if(!inputCommand.equals("G")&&!inputCommand.equals("R")&&!inputCommand.equals("W")&&!inputCommand.equals("E"))
				throw new NoSuchCommandExceptions("[error]指令"+inputCommand+"錯了！！！\n沒有此指令");
				
			
			switch (inputCommand.charAt(0)) {
				case 'G':
					aGradeSystem.showGrade(ID);
					break;
				case 'R':
					aGradeSystem.showRank(ID);
					break;
				case 'W':
					aGradeSystem.updateWeights();
					break;
				case 'E':
					exitFlag = true;
					break;
				default:
					break;
			}//end of switch
		
		}//end of while
		return 0;
	}//end of function

	/**
	 * Use Scanner to get UserID
	 * @return ID
	 */
	public String promptID() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.printf("輸入ID 或 Q (結束使用)\n");
		System.out.printf(">");
		ID = scanner.nextLine();
		
		return ID;
	}
	
	/**
	 * show finish GradeSystem Message
	 */
	public void showFinishMsg() {
		System.out.printf("thanks! bye~\n");
	}
	
	/**
	 * show welcome message
	 */
	public void showWelcomeMsg() {
		System.out.printf("Welcome %s to use GradeSystem v1.0\n", ID);
	}

}
