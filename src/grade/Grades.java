package grade;

/**
 * 
 * class Grades 儲存 ID, name, lab1, lab2, lab3, midTerm, finalExam, and totalGrade
 *
 * Grades(){} //建構子
 * calculateTotalGrade(weights){totalGrade須四捨五入}
 * 
 */
public class Grades {

	private String name;
	private String id;
	public int [] gradeItem; 
	
	
	public int totalGrade;
	

	/**
	 * initial Grade private variable
	 */
	public Grades() {
		
		name = "";
		setId("0");
		totalGrade = 0;
		
		gradeItem = new int[5];
		java.util.Arrays.fill(gradeItem, 0); // 讓grade先預設為0
		
		
	}; // end of constructor
	
	/**
	 * Initial Grade private with parameter
	 * @param lab1
	 * @param lab2
	 * @param lab3
	 * @param midTerm
	 * @param finalTerm
	 */
	public Grades(int lab1,int lab2,int lab3,int midTerm, int finalTerm){
		
		name = "";
		setId("0");
		totalGrade = 0;
		
		gradeItem = new int[5];
		
		
		gradeItem[0] = lab1;
		gradeItem[1] = lab2;
		gradeItem[2] = lab3;
		gradeItem[3] = midTerm;
		gradeItem[4] = finalTerm;
		
	}
	
/**
 * set Grade value
 * 
 * @param index
 * @param value //grade
 */
	public void setGradeItem(int index, String value){	
		gradeItem[index] = Integer.parseInt(value);
	}

/**
 * 
 * 計算totalGrade
 * 
 * @param weight
 * @return totalGrade
 * 
 * @code
 * 	1.依照輸入的weight計算其成績 </br>
 *  2.回傳四則五入的totalGrade </br>
 */
	
	public int calculateTotalGrade(double [] weight) {
		double sum = 0.0;
		for (int i = 0; i < weight.length; i++) {	

			sum +=   gradeItem[i] * weight[i];
		}
		totalGrade = (int)Math.round(sum);
		return totalGrade;  //四則五入
		
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	};
	
	public String getName(){
		return this.name;
	}
}
