package test;

import static org.junit.Assert.*;

import grade.Grades;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class testCalculateTotalGrade {

	private Grades aGrade;
	
	@Before
	public void setUp() throws Exception {
		aGrade = new Grades(81,98,84,90,93);
	}

	@After
	public void tearDown() throws Exception {
		aGrade = null;
	}



	@Test //(expected=NoSuchIDExceptions.class)
	public void testCalculateTotalGrade1() {
		
		double [] weights = {0.1,0.1,0.1,0.3,0.4}; 
		
		assertEquals(91, aGrade.calculateTotalGrade(weights));
		
	}
	
	@Test
	public void testCalculateTotalGrade2() {
		
		double [] weights = {0.5,0.2,0.1,0.1,0.1}; 
		
		assertEquals(87, aGrade.calculateTotalGrade(weights));
		
	}
	
	@Test
	public void testCalculateTotalGrade3() {
		
		double [] weights = {0.3,0.3,0.1,0.1,0.2}; 
		
		assertEquals(90, aGrade.calculateTotalGrade(weights));
		
	}
	
}
