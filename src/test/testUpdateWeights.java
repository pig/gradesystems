package test;

import static org.junit.Assert.*;

import grade.GradeSystems;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class testUpdateWeights {
	private GradeSystems aGradeSystem;
	private String inputYes = "10\r\n20\r\n30\r\n20\r\n20\r\nyes\r\n";
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	@Before
	public void setUp() throws Exception {
		System.setIn(new ByteArrayInputStream(inputYes.getBytes()));
		System.setOut(new PrintStream(outContent));
		System.setOut(new PrintStream(errContent));
		aGradeSystem = new GradeSystems();
	}

	@After
	public void tearDown() throws Exception {
		aGradeSystem = null;
		System.setErr(null);
		System.setOut(null);
		System.setIn(null);
	}

	@Test
	public void testYes() {
		assertTrue(aGradeSystem.updateWeights());
		//fail("Not yet implemented");
	}

}
