package test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import grade.GradeSystems;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class testShowGrade {
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	private GradeSystems aGradeSystem;
	
	@Before
	public void setUp() throws Exception {
		System.setOut(new PrintStream(outContent));
		System.setErr(new PrintStream(errContent));
		aGradeSystem = new GradeSystems();		
	}

	@After
	public void tearDown() throws Exception {
		aGradeSystem = null;
		System.setErr(null);
		System.setOut(null);
	}

	@Test
	public void testShowGradeOut() {
		aGradeSystem.showGrade("985002033");
		String expectedStr = "�L�v޳���Z:\nlab1: \t90\nlab2: \t88\nlab3: \t97\nmid-term: \t91\nfinal exam: \t94\ntotal grade: \t92\n";
		assertEquals(expectedStr, outContent.toString());
	}
	

}
