package test;

import static org.junit.Assert.*;

import grade.UI;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class testShowWelcomeMsg {
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private UI aUI;
	@Before
	public void setUp() throws Exception {
		System.setIn(new ByteArrayInputStream("985002006\n".getBytes()));
		aUI = new UI();
		System.setOut(new PrintStream(outContent));
	}

	@After
	public void tearDown() throws Exception {
		System.setIn(null);
		System.setOut(null);
		aUI = null;
	}

	@Test
	public void test() {
		assertEquals("Welcome 985002006 to use GradeSystem v1.0\n", outContent.toString());
	}

}
