package test;

import static org.junit.Assert.*;

import grade.GradeSystems;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class testShowRank {
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	private GradeSystems aGradeSystem;
	
	@Before
	public void setUp() throws Exception {
		System.setOut(new PrintStream(outContent));
		System.setErr(new PrintStream(errContent));
		aGradeSystem = new GradeSystems();
	}

	@After
	public void tearDown() throws Exception {
		aGradeSystem = null;
		System.setErr(null);
		System.setOut(null);
	}

	/*
	 * 需要手動算 rank 是多少
	 * for inputFile3
	 * No.01 985002509	蔡宗衛	84	92	98	94	99	95.2
	 * No.34 985002006	楊佳蓉	82	88	85	89	92	89
	 * No.65 985002513	黃靖崴	85	86	94	80	85	84.5
	 * 
	 * */
	@Test
	public void testTopRank() {
		aGradeSystem.showRank("985002509");
		String expectedStr = "蔡宗衛排名第1\n";
		assertEquals(expectedStr, outContent.toString());
		//fail("Not yet implemented");
	}
	@Test
	public void testMidRank() {
		aGradeSystem.showRank("985002006");
		String expectedStr = "楊佳蓉排名第32\n";
		assertEquals(expectedStr, outContent.toString());
	}
	@Test
	public void testLastRank() {
		aGradeSystem.showRank("985002513");
		String expectedStr = "黃靖崴排名第60\n";
		assertEquals(expectedStr, outContent.toString());
	}
	

}
