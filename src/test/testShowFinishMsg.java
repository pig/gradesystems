package test;

import static org.junit.Assert.*;

import grade.UI;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class testShowFinishMsg {
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private UI aUI;

	@Before
	public void setUp() throws Exception {
		System.setIn(new ByteArrayInputStream("Q\n".getBytes()));
		System.setOut(new PrintStream(outContent));
		aUI = new UI();
	}

	@After
	public void tearDown() throws Exception {
		aUI = null;
		System.setOut(null);
		System.setIn(null);
	}

	@Test
	public void test() {
		assertEquals("輸入ID 或 Q (結束使用)\n>thanks! bye~\n", outContent.toString());
	}

}
